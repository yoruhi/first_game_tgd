package fr.tgd.menus;

import java.awt.Font;


import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;





import fr.tgd.main.WorldGenGame;

public class Game extends StateBasedGame {
	

	public static void main(String[] args) throws SlickException {
		new AppGameContainer(new Game(), 800, 600, false).start();
	}

	public Game() {
		super("CodingNight");
	}
	
	static TrueTypeFont font1;
	static TrueTypeFont font2;
	static TrueTypeFont font3;
	static TrueTypeFont font4;
	static TrueTypeFont font5;
	static TrueTypeFont font6;
	static TrueTypeFont font7;
	static TrueTypeFont font8;
	static TrueTypeFont font9;
	static TrueTypeFont font0;

	@Override
	public void initStatesList(GameContainer container) throws SlickException {

		addState(new MainMenu());
		addState(new GOMenu());
		addState(new PauseMenu());
		addState(new WorldGenGame());
		addState(new HelpMenu());
		addState(new ScoreMenu());
		addState(new ConfirmMenu());

	}
	
	

	
	
}