package fr.tgd.menus;


import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import fr.tgd.main.WorldGenGame;
import fr.tgd.menus.MainMenu;
import fr.tgd.world.Character;


public class ConfirmMenu extends BasicGameState {
	
	public static int ID = 8;

	private String nom = "Etes-vous s�r(e)? Les scores seront perdus";
	private String[] items = { "Non", "Oui" };

	public int nbrOption = items.length;

	public String[] getItems() {
		return this.items;
	}

	static StateBasedGame game;

	static GameContainer container;
	int selection = 0;

	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		this.game = game;
		this.container = container;
		container.setShowFPS(false);
		
		Font titre9Font = new Font("Courant", Font.BOLD, 22);
    	Game.font9 = new TrueTypeFont(titre9Font, false);
    	
    	Font titre0Font = new Font("Courant", Font.BOLD, 16);
    	Game.font0 = new TrueTypeFont(titre0Font, false);
    	
	}

	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {

	}

	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {

		g.setColor(Color.white);
		g.setFont(Game.font9);

		g.drawString(this.nom, 200, 200);

		g.setFont(Game.font0);

		for (int i = 0; i < nbrOption; i++) {
			g.drawString(this.items[i], 300, 280 + 50 * i);
		}

		g.drawString(">>", 230, 280 + 50 * selection);

	}

	public void keyPressed(int key, char c) {
		switch (key) {
		case Input.KEY_DOWN:
			if (selection < nbrOption - 1)
				selection++;
			else
				selection = 0;
			break;
		case Input.KEY_UP:
			if (selection > 0)
				selection--;
			else
				selection = nbrOption - 1;
			break;
		case Input.KEY_ENTER:
			execOption();
			break;
		case Input.KEY_ESCAPE:
			game.enterState(MainMenu.ID);
			break;
		}
	}

	public void execOption() {

		switch (selection) {
		case 0:
			game.enterState(MainMenu.ID);
			break;
		case 1:
			container.exit();
			break;

		}
	}
	public int getID() {
		return ID;
	}

}
